---
title: Privacy Policy
metaTitle: Privacy Policy | Psono Documentation
meta:
  - name: description
    content: The legal notice
sidebar: false
---

# Privacy Policy


Your privacy is important to us. Psono follows some fundamental principles:

We do not ask for any information that we do not really need
We do not share your information with anyone except to comply with the German law, protect our rights and develop our product.
No sensitive data like your passwords will ever leave your computer without being encrypted before
Your personal data is processed by us only in accordance with the German data privacy law. The following passages
describes the type and purpose of the use of the gathered personal data.
This privacy policy only applies to "doc.psono.com" and no other linked domains.

## Contact Details:

If you are looking to contact the body that is responsible for the data privacy then please reach out to:

esaqa GmbH  
Tiergartenstr. 13  
91247 Vorra  
Germany

Managing Director: Sascha Pfeiffer  
Commercial Register: Nuremberg Registry Court HRB 37978  
USt-IdNr.: DE333156173  

E-mail: support@esaqa.com  


## Personal Data

Any data (name, address, billing information, IP-address, …) is only used to fulfill the service and without a legal basis or your explicit consent, not given to any third parties.

Psono collects personal-identifying information like for example the IP address while using our service, to ensure the service availability and to protect your account. Psono may use that information for statistical analysis and may display this anonymized statistical analysis publicly or provide it to others.

Account holders in addition must specify some additional information during registration, like for example their e-mail address. That information is used to prevent service disruptions and to inform users about security relevant information.


## What User Data We Collect

When you visit the website, we may collect the following data:

- Your IP address

- Your browser / OS and version

- Your language

- Date and time of the request

- Requested resource / website

- Transfered data

- Status code of the request

- Website who refered you


## Why We Collect Your Data

We are collecting your data for several reasons:

- To better understand your needs.

- To improve our services and products.

- To protect our system against attacks and fraud


## Your rights

You have the following rights about your personal data:

- Right to be informed

- Right of access

- Right to rectification

- Right to erasure

- Right to restrict processing

- Right to data portability

- Right to object

- Rights related to automated decision making including profiling

You further have the right to make a complaint to the ICO or another supervisory authority


## Services

Services that we use and that store and handle your data:


### Browser Storage

For technical reason (login, sessions, ...) we are using browser storage (e.g. cookies, local storage, ...), which stores data in your browser.


### Mailgun

This website is using Mailgun as a service to handle all e-mail traffic, that includes registration emails, support
emails and potential security notifications.


### Freshdesk

This website uses Freshdesk to handle support requests. Whenever you are opening a new ticket, via email or support
form it will be processes by Freshdesk including all information that you may provide in the process (IP, email address,
all data of your support request, ...).


### Cloudflare


Psono is hosted by Cloudflare. All requests are handled by servers that are closest to the user. An overview of Cloudflares data
privacy commitment can be found here in [Cloudflare's privacy policy](https://www.cloudflare.com/privacypolicy/)


Psono may update its privacy statement from time to time and we encourage everyone to check regularly.
It has last been updated Feb 28th, 2021.
