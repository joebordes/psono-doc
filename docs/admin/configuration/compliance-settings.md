---
title: Compliance Settings
metaTitle: Compliance Settings | Psono Documentation
meta:
  - name: description
    content: Guide to explain common compliance settings.
---

# Compliance Settings

## Preamble

This guide will explain the various compliance settings that Psono offers in order to fulfill various security and compliance standards (HIPAA, ISO27001, PCI-DSS, ...).

::: tip
All features starting with `COMPLIANCE_` are only available in the Enterprise Edition.
:::

## Settings

### Enforce central security reports

If you have the requirement to audit your user's passwords and check passowrds for data breaches and so on, then this
setting comes in handy. It allows you to force your users to send their security report to the server so an admin
or auditor can analyze it.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_ENFORCE_CENTRAL_SECURITY_REPORTS: True
```
Restart the server afterwards

### Enforce regular security reports

If you have the requirement that your user's audit their passwords regulary, then you can force them to do so in a
fix interval.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_CENTRAL_SECURITY_REPORT_SECURITY_RECURRENCE_INTERVAL: 2592000
```
(The value is in seconds, so 2592000 seconds = 30*24*3600 seconds = 30 days)
A value of 0 would disable it. Restart the server afterwards

### Enforce second factor

If you have the requirement to enforce a second factor for all users, you can do so with this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_ENFORCE_2FA: True
```
Restart the server afterwards

### Disable exports

Maybe you don't want to allow your users to export their passwords. Then you can enforce this restriction with this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_EXPORT: True
```
Restart the server afterwards

### Disable self deletion

Maybe you don't want to allow your users to delete their own account. You can achieve this with this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_DELETE_ACCOUNT: True
```
Restart the server afterwards

### Disable api keys

Maybe you don't want to allow your users to bypass second factor restriction with API keys, then you can disable
them with this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_API_KEYS: True
```
Restart the server afterwards

### Disable emergency codes

This setting allows you to disable emergency codes.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_EMERGENCY_CODES: True
```
Restart the server afterwards

### Disable recovery codes

This setting allows you to disable recovery codes.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_RECOVERY_CODES: True
```
Restart the server afterwards

### Disable file repositories

Maybe you have the requirement that all file exchange need to go through an antivirus scanner or just don't want
to provide your users the possibility to bypass internet proxies that are configured to prevent data exfiltration.
This setting allows you to disable file repositories.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_FILE_REPOSITORIES: True
```
Restart the server afterwards

### Disable link shares

Link shares allow users to share content with "not registered" users, as such it's harder to audit access. This
setting allows you to disable link shares.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_LINK_SHARES: True
```
Restart the server afterwards

### Disable offline mode

You may want to restrict users to create an "offline copy" and as such enter "offline mode". This setting allows you
to disable the offline mode.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_OFFLINE_MODE: True
```
Restart the server afterwards

### Enforce minimum password length

One of the classics. This feature allows you to enforce a minimum password length.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_MIN_MASTER_PASSWORD_LENGTH: 12
```
Restart the server afterwards

### Enforce minimum password complexity

Another classic. This feature allows you to enforce how many security groups the password needs to pass in order to
be rated complex enough. Psono security groups distinguish between lowercase, uppercase, numbers, special chars.
So a setting of 3 would require that a password matches at least 3 of the mentioned 4 security groups.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_MIN_MASTER_PASSWORD_COMPLEXITY: 3
```
Restart the server afterwards

### Logout after inactivity

Another classic. You can enforce an automatic logout after a certain time of inactivity. This setting can be used
to configured a timer that resets with every interaction and allows the session to be active for another X seconds.

Configure in your `settings.yml` the following line
```yaml
AUTO_PROLONGATION_TOKEN_TIME_VALID: 900
```
(The value is in seconds, so 900 seconds = 15*60 seconds = 15 minutes)
Restart the server afterwards


### Multiple sessions

Maybe you need to prevent that a user is logged in with multiple devices. Then you can prevent multiple sessions
with the setting below, forcing a user to be logged out on other devices once he logs in.

Configure in your `settings.yml` the following line
```yaml
ALLOW_MULTIPLE_SESSIONS: False
```
Restart the server afterwards

### Max session lifetime of webclient sessions

Allows you to restrict the time that a session from a webclient (and extensions) is valid.

Configure in your `settings.yml` the following line
```yaml
MAX_WEBCLIENT_TOKEN_TIME_VALID: 2592000
```
(The value is in seconds, so 2592000 seconds = 30*24*3600 seconds = 30 days)
Restart the server afterwards

::: tip
This feature will not work together with AUTO_PROLONGATION_TOKEN_TIME_VALID
:::


### Max session lifetime of app sessions

Allows you to restrict the time that a session from an app is valid.

Configure in your `settings.yml` the following line
```yaml
MAX_APP_TOKEN_TIME_VALID: 31536000
```
(The value is in seconds, so 31536000 seconds = 365*24*3600 seconds = 365 days)
Restart the server afterwards

::: tip
This feature will not work together with AUTO_PROLONGATION_TOKEN_TIME_VALID
:::


### Max session lifetime of api key sessions

Allows you to restrict the time that a session from an api key is valid.

Configure in your `settings.yml` the following line
```yaml
MAX_API_KEY_TOKEN_TIME_VALID: 600
```
(The value is in seconds, so 600 seconds = 10*60 seconds = 10 minutes)
Restart the server afterwards

::: tip
This feature will not work together with AUTO_PROLONGATION_TOKEN_TIME_VALID
:::


### Prevent old passwords to be reused

Allows you to restrict old passwords so they cannot be reused. This setting allows you to specify how many of their last
passwords should be blocked.

Configure in your `settings.yml` the following line
```yaml
DISABLE_LAST_PASSWORDS: 3
```
Restart the server afterwards
