---
title: LDAPS - Custom CA
metaTitle: LDAPS with custom CA | Psono Documentation
meta:
  - name: description
    content: Configuration of LDAPS with a custom certificate authority
---

# LDAPS with custom CA

## Preamble

The EE server and client support the LDAP protocol that allows you to configure an external LDAP service for authentication.
This guide here will explain how to configure Psono Server to accept a custom CA signed certificate with ldaps

We assume that your LDAP server is accessible under `ldaps://someIPA.domain.local:636` with ldaps using a custom CA.
We further assume that you have your ca.crt file located in `/path/to/ca.crt`, containing the root CA crt and maybe intermediate certificates.

::: tip
This feature is only available in the Enterprise Edition.
:::

## Server (settings.yaml)

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Adjust the setting.yml as shown below

	```yaml
	LDAP : [
      {
        ...
        'LDAP_URL': 'ldaps://someIPA.domain.local:636',
        'LDAP_CA_CERT_FILE': '/root/.psono_server/ca.crt',
      },
    ]
	```

	- Make sure that the "authority" of the configured LDAP_URL (in the example above `someIPA.domain.local:636`) is part of the domains listed in the certificate that is presented by the server.
	- The `LDAP_CA_CERT_FILE` path is the path within the docker container. It needs to match the mount point in the section below. Leave it as it is if you are not sure.

2.  Test your integration

    To test the settings you can use the `testldap` command like shown below

	```bash
    docker run --rm \
      -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
      -v /path/to/ca.crt:/root/.psono_server/ca.crt \
      -ti psono/psono-server-enterprise:latest python3 ./psono/manage.py testldap username@something.com thePassWord
	```


    ::: tip
    Once you have a working configuration don't forget to restart the server.
    :::

## Start server

1.  Adjust server start command

    Normally you would start the server with this command:

    ```
    docker run --name psono-server-enterprise \
        --sysctl net.core.somaxconn=65535 \
        -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
        -v /path/to/log/folder:/var/log/psono \
        -d --restart=unless-stopped -p 10100:80 psono/psono-server-enterprise:latest
    ```

    You have to mount your ca.crt with this parameter:

        -v /opt/psono/ca.crt:/root/.psono_server/ca.crt

    So it would look like this:

        docker run --name psono-server-enterprise \
            --sysctl net.core.somaxconn=65535 \
            -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
            -v /path/to/log/folder:/var/log/psono \
            -v /path/to/ca.crt:/root/.psono_server/ca.crt \
            -d --restart=unless-stopped -p 10100:80 psono/psono-server-enterprise:latest

    Adjust `/path/to/ca.crt` to match your path.

