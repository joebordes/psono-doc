---
title: SAML - Okta
metaTitle: Okta as SAML IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of Okta as SAML IDP
---

# Okta as SAML IDP for SSO

## Preamble

The EE server and client support the SAML protocol that allows you to configure an external service as IDP (identity provider)
for SSO (single sign on).
This guide here will explain how to configure Okta as SAML IDP for SSO.
We assume that your webclient is running on https://example.com, the server is reachable with
`https://example.com/server` (e.g. `https://example.com/server/info/` shows you some nice json output). This is your first
SAML provider that you want to configure (therefore we give him the ID "1").


::: tip
This feature is only available in the Enterprise Edition.
:::

## Okta

As a first step we have to configure Okta.

1.  Go to Applications

    ![Got to applications](/images/admin/configuration/saml_okta_go_to_applications.jpg)

2.  Click "Add Application"

    ![Click "Add application"](/images/admin/configuration/saml_okta_add_application.jpg)

3.  Click "Create New App"

    ![Click "Create New App"](/images/admin/configuration/saml_okta_create_new_app.jpg)

4.  Select "Web" and "Saml 2.0"

    ![Select "Web" and "Saml 2.0"](/images/admin/configuration/saml_okta_select_web_and_saml20.jpg)


5.  Enter Name in "General Settings"

    ![Enter Name in "General Settings"](/images/admin/configuration/saml_okta_general_settings.jpg)


6.  Configure SAML

    Configure SAML as shown below. Adjust the URLs according to your domain (and if necessary according to your SAML provider ID)

    ![Configure SAML](/images/admin/configuration/saml_okta_configure_saml.jpg)

7.  Feedback

    Click "I'm an Okta customer adding an internal app" and fill in the necessary information in order to pass this screen.

    ![Feedback](/images/admin/configuration/saml_okta_feedback.jpg)

8.  Click "View Setup Instructions"

    ![Click "View Setup Instructions"](/images/admin/configuration/saml_okta_click_view_setup_instructions.jpg)

8.  Remember "Setup Instructions"

    You should see now a screen as shown below.

    ![Click "View Setup Instructions"](/images/admin/configuration/saml_okta_setup_instructions.jpg)

    ::: tip
    Download / Remember these information, you will need them in the next step.
    :::


8.  Go to "Assignments"

    Assign users and groups who should be able to use Psono.

    ![Assign users and groups](/images/admin/configuration/saml_okta_assign_users_and_groups.jpg)


## Server (settings.yaml)

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Generate SP certificate

    You will need a certificate for your service provider (SP) later. You can generate one easily with:

    ```bash
    openssl req -new -newkey rsa:2048 -x509 -days 3650 -nodes -sha256 -out sp_x509cert.crt -keyout sp_private_key.key
    ```

    This will generate a private key (sp_private_key.key) and the public certificate (sp_x509cert.crt).

2.  Comment in the following section:

	```yaml
	SAML_CONFIGURATIONS:
	    1:
	        idp:
	            entityId: "OKTA_SSO_SAML_IDENTITY_PROVIDER_ISSUE"
	            singleLogoutService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	                url: ""
	            singleSignOnService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	                url: "OKTA_SSO_SIGN_IN_URL"
	            x509cert: "OKTA_SSO_X509_CERTIFICATE"
	            groups_attribute: "groups"
	            username_attribute: "username"
	            email_attribute: "email"
	            username_domain: "example.com"
	            required_group: []
	            is_adfs: false
	            honor_multifactors: true
	            max_session_lifetime: 43200
	        sp:
	            NameIDFormat: "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"
	            assertionConsumerService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"
	            attributeConsumingService:
	                serviceName: "SP_SERVICE_NAME"
	                serviceDescription: "Psono password manager"
	                requestedAttributes:
	                    -
	                        attributeValue: []
	                        friendlyName: ""
	                        isRequired: false
	                        name: "attribute-that-has-to-be-requested-explicitely"
	                        nameFormat: ""
	            privateKey: "SP_PRIVATE_CERTIFICATE"
	            singleLogoutService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	            x509cert: "SP_X509CERT"
	        strict: true
	```

	- Replace `OKTA_SSO_SAML_IDENTITY_PROVIDER_ISSUE` with the "Identity Provider Issuer" from Okta's previous "Setup Instructions".
	- Replace `OKTA_SSO_SIGN_IN_URL` with the "Identity Provider Single Sign-On URL" from kta's previous "Setup Instructions".
	- Replace `OKTA_SSO_X509_CERTIFICATE` with the "X.509 Certificate" from Okta's previous "Setup Instructions". (remove all line breaks)
	- Replace `SP_SERVICE_NAME` with the "metadata" URL of your service provider, e.g. https://example.com/server/saml/1/metadata/
	- Replace `SP_PRIVATE_CERTIFICATE` with the content of the previous generated "sp_private_key.key". (remove all line breaks)
	- Replace `SP_X509CERT` with the content of the previous generated "sp_x509cert.crt". (remove all line breaks)

    Restart the server afterwards

3.  Ajust authentication methods

    Make sure that `SAML` is part of the `AUTHENTICATION_METHODS` parameter in your settings. e.g.

	```yaml
	AUTHENTICATION_METHODS: ['SAML']
	```
    Restart the server afterwards



4.  (optional) Debug Mode

    It is helpful in the later debugging to enable debug mode.

	```yaml
	DEBUG: True
	```

    ::: warning
    Restart the server afterwards and don't forget to remove it before going to production.
    :::


## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.

1.  Edit config.json

    Update your config.json similar to the one shown below.

	```json
	{
	  ...
      "authentication_methods": ["SAML"],
      "saml_provider": [{
        "title": "Some text before the button. e.g. Company Login",
        "provider_id": 1,
        "button_name": "SAML SSO Login"
      }]
	  ...
	}
	```

	The variable authentication_methods restricts the allowed login methods. In the example above only SAML will be allowed
	and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
	to match the one that you used on your server.


