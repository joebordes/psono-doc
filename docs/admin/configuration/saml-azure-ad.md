---
title: SAML - Azure AD
metaTitle: Azure AD as SAML IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of Azure AD as SAML IDP
---

# Azure AD as SAML IDP for SSO

## Preamble

The EE server and client support the SAML protocol that allows you to configure an external service as IDP (identity provider)
for SSO (single sign on).
This guide here will explain how to configure Azure AD as SAML IDP for SSO. We assume that Azure AD can firewall / network wise access your server.
In addition we assume that your webclient is running on https://example.com, the server is reachable with
`https://example.com/server` (e.g. `https://example.com/server/info/` shows you some nice json output). This is your first
SAML provider that you want to configure (therefore we give him the ID "1").
Azure AD allows in all Versions up to 10 SSO Applications. Also if you are using Office 365 it is included. (See: [azure.microsoft.com/en-us/pricing/details/active-directory/](https://azure.microsoft.com/en-us/pricing/details/active-directory/)

::: tip
This feature is only available in the Enterprise Edition.
:::

## Azure Active Directory

As a first step we have to configure Azure Active Directory.

1.  Enable Azure AD SSO

    Go to your [Azure AD Enterprise Applications](https://aad.portal.azure.com/#blade/Microsoft_AAD_IAM/StartboardApplicationsMenuBlade/AllApps/menuId/).

    ![Enable Azure AD SSO](/images/admin/configuration/saml_azure_enable_azure_ad_sso.jpg)

2. Create Application

    Click on "New Application" and choose "Non-gallery application". Select a Name and click on "Add".

    ![Create Application](/images/admin/configuration/saml_azure_create_application.jpg)

    ::: tip
    If you choose an other Name except 'Psono', you need to configure this in the settings.yaml
    :::

2. Configure Application Permissions

    Click on "1. Assign users and groups" to assign Users or security groups (Office 365 Groups aren't working) to access PSONO via SAML.

    ![Configure Application Permissions](/images/admin/configuration/saml_azure_configure_application_permissions.jpg)

3. Configure SAML on the Application

    Click on "2. Set up single sign on" and then choose "SAML".

    ![Configure SAML on the Application Step 1](/images/admin/configuration/saml_azure_configure_saml_application_step1.jpg)

    Configure the steps as shown in the picture below and save it. Copy the marked lines.

    ![Configure SAML on the Application Step 2](/images/admin/configuration/saml_azure_configure_saml_application_step2.jpg)

## Server (settings.yaml)

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Generate SP certificate

    You will need a certificate for your service provider (SP) later. You can generate one easily with:

    ```bash
    openssl req -new -newkey rsa:2048 -x509 -days 3650 -nodes -sha256 -out sp_x509cert.crt -keyout sp_private_key.key
    ```

    This will generate a private key (sp_private_key.key) and the public certificate (sp_x509cert.crt).

2.  Change or add SAML configuration in to settings.yaml.
    Comments inside the config:

	```yaml
	SAML_CONFIGURATIONS:
	    1:
	        idp:
	            entityId: "REPLACE_WITH_AZURE_AD_IDENTIFIER"
	            singleLogoutService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	                url: "https://login.microsoftonline.com/common/wsfederation?wa=wsignout1.0"
	            singleSignOnService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	                url: "REPLACE_WITH_LOGIN_URL"
	            x509cert: "CERT_FROM_AZURE_APPLICATION"
	            groups_attribute: "groups"
	            username_attribute: "username"
	            email_attribute: "email"
	            username_domain: "USER_DOMAIN"
	            required_group: []
	            is_adfs: true
	            honor_multifactors: true
	            max_session_lifetime: 43200
	        sp:
	            NameIDFormat: "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"
	            assertionConsumerService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"
	            attributeConsumingService:
	                serviceName: "Psono"
	                serviceDescription: "Psono password manager"
	                requestedAttributes:
	                    -
	                        attributeValue: []
	                        friendlyName: ""
	                        isRequired: false
	                        name: "username"
	                        nameFormat: ""
	            privateKey: "SP_PRIVATE_CERTIFICATE"
	            singleLogoutService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	            x509cert: "SP_X509CERT"
	        strict: true
	        security:
	            requestedAuthnContext: false
	```

	- Change `serviceName` to match the name of your Azure's custom application
	- Change `USER_DOMAIN` to match your domain.tld
	- Replace `REPLACE_WITH_LOGIN_URL` with the "Login Url" usually something like https://login.microsoftonline.com/...
	- Replace `REPLACE_WITH_AZURE_AD_IDENTIFIER` with the Azure AD identifier usually something like https://sts.windows.net/...
	- Replace `CERT_FROM_AZURE_APPLICATION` with the "Certificate (Base64)" from previous Azure configuration. (remove all line breaks)
	- Replace `SP_PRIVATE_CERTIFICATE` with the content of the previous generated "sp_private_key.key". (remove all line breaks)
	- Replace `SP_X509CERT` with the content of the previous generated "sp_x509cert.crt". (remove all line breaks)

    Restart the server afterwards

3.  Ajust authentication methods

    Make sure that `SAML` is part of the `AUTHENTICATION_METHODS` parameter in your settings. e.g.

	```yaml
	AUTHENTICATION_METHODS: ['SAML']
	```
    Restart the server afterwards

4.  (optional) Debug Mode

    It is helpful in the later debugging to enable debug mode.

	```yaml
	DEBUG: True
	```

    ::: warning
    Restart the server afterwards and don't forget to remove it before going to production.
    :::

## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.

1.  Edit config.json

    Update your config.json similar to the one shown below.

	```json
	{
	  ...
      "authentication_methods": ["SAML"],
      "saml_provider": [{
        "title": "Some text before the button. e.g. Company Login",
        "provider_id": 1,
        "button_name": "SAML SSO Login"
      }]
	  ...
	}
	```

	The variable authentication_methods restricts the allowed login methods. In the example above only SAML will be allowed
	and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
	to match the one that you used on your server.
	If the webclient won't show any login buttons and has a loading loop, maybe something went wrong in the "config.json". Please check if syntax seems correct.


