---
title: Email - SparkPost
metaTitle: Email configuration with SparkPost | Psono Documentation
meta:
  - name: description
    content: Configuration of email delivery with SparkPost
---

# Email configuration with SparkPost

## Preamble

The server supports multiple email providers. This guide will explain how to configure the Psono server to use SparkPost for
email delivery.

## Configuration

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Configure email address

	```
	EMAIL_FROM: 'something@example.com'
	```

	All emails that are sent by the server will come from this email address.

    Restart the server afterwards

2.  Add SparkPost API credentials to setting.yml

	```
	EMAIL_BACKEND: 'anymail.backends.sparkpost.EmailBackend'
    SPARKPOST_API_KEY: 'YOUR_SPARKPOST_API_KEY'
	```

    Replace `YOUR_SPARKPOST_API_KEY` with the API key that was provided to you by SparkPost.

    Restart the server afterwards


3.  (optional) EU Server

    If you are using the EU server, you have to specify in addition

	```
	SPARKPOST_API_URL: 'https://api.eu.sparkpost.com/api/v1'
	```

    Restart the server afterwards


## Testing

To send a test email to `something@something.com` execute:

    python3 ./psono/manage.py sendtestmail something@something.com

or with docker:

    docker run --rm \
      -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-server:latest python3 ./psono/manage.py sendtestmail something@something.com

If you receive this test email, then email should be configured proper.


## More Information

Psono is using Anymail under the hood. You can check out the official documentation here:

[anymail.readthedocs.io/en/stable/esps/sparkpost/](https://anymail.readthedocs.io/en/stable/esps/sparkpost/)

