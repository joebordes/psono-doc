---
title: SAML - Keycloak
metaTitle: Keycloak as SAML IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of Keycloak as SAML IDP
---

# Keycloak as IDP for SAML-SSO

To set up the IDP you need a running instance of Keycloak with a configurable realm. Let's pretend it is called `my_realm`.

## Preamble

The EE server and client support the SAML protocol that allows you to configure an external service as IDP (identity
provider) for SSO (single sign on). This guide here will explain how to configure Keycloak as SAML-IDP for SSO. We
assume that your Keycloak instance is running on `https://keycloak.example.com`, your webclient on
`https://psono.example.com` and finally the server is reachable with `https://psono.example.com/server` (e.g.
`https://example.com/server/info/` shows you some nice json output). This is your first SAML provider that you want to
configure (therefore we give him the ID "1").

::: tip
This feature is only available in the Enterprise Edition.
:::

## Keycloak

1. Navigate to your realm and click on `Create` in the `Clients`-Section.
2. In there add your new client like shown below

    ![client-create](/images/admin/configuration/saml_keycloak_client-create.png)

3. Afterwards adapt the settings of the client. Don't forget to click on save at the bottom of the page.

    1. Give it a name and a short description if you wish.
    2. Choose your algorithm (this will be relevant later on).
    3. Choose your Name ID Format (email works perfectly fine).
    4. Configure the `Valid Redirect URL` (Wildcard in this case).

    ![client-config](/images/admin/configuration/saml_keycloak_client-config.png)

4. Now a new tab called `SAML Keys` should show up. Go there and copy the certificate (5) and private key (6) for the later use.
5. In the `Client Scopes`-tab you have to remove the default client scope (we will create our own).

    ![client-scopes](/images/admin/configuration/saml_keycloak_client-scopes.png)

6. Next in the `Mappers`-tab click on `Create` in the top-right corner. Make sure to check `Single Role Attribute`.

    ![client-mapper-roles](/images/admin/configuration/saml_keycloak_client-mapper-roles.png)

7. Create another mapper for the mail address as the following picture suggests. Also add yet another mapper for the attribute you chose at (3).

    ![client-mapper-email](/images/admin/configuration/saml_keycloak_client-mapper-email.png)

8. Within my_realm go to the `Realm Settings`-section and then in the `Keys`-tab. Depending on you choice for (2) copy the certificate (9).

    ![realm-keys](/images/admin/configuration/saml_keycloak_realm-keys.png)

## Server (settings.yaml)

After setting up the IDP for the SAML-Authentication it is time to configure your running Psono-Server to act as the SP.
It is required that Psono can reach Keycloak and vise versa.

Edit the settings.yml like so:

```yml
# Make sure the 'SAML'-entry is present in the following list
AUTHENTICATION_METHODS: ['SAML']

SAML_CONFIGURATIONS:
    1:
        idp:
            entityId: "https://keycloak.example.com/auth/realms/my_realm"
            singleLogoutService:
                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
                url: "https://keycloak.example.com/auth/realms/my_realm/protocol/saml"
            singleSignOnService:
                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
                url: "https://keycloak.example.com/auth/realms/my_realm/protocol/saml"
            x509cert: "{certificate from (9)}"
            groups_attribute: "{key from (7)}"
            username_attribute: "{key depending on your choice on (3) and (8)}"
            email_attribute: "{key from (8)"
            username_domain: "example.com"
            # If you have only certain user that may access Psono, create a role, put it in here and assign it to the users.
            required_group: ["Psono Role"]
            is_adfs: false
            honor_multifactors: true
            max_session_lifetime: 86400
        sp:
            entityId: "https://psono.example.com/server/saml/1/metadata/"
            # depends on your choice for (3). See https://github.com/onelogin/python3-saml/blob/master/src/onelogin/saml2/constants.py for all values.
            NameIDFormat: "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"
            clientId: "https://psono.example.com/server/saml/1/metadata/"
            assertionConsumerService:
                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"
            attributeConsumingService:
                serviceName: "Psono"
                serviceDescription: "Psono EE password manager"
                requestedAttributes:
                    -
                        attributeValue: []
                        friendlyName: ""
                        isRequired: false
                        name: "email"
                        nameFormat: ""
            x509cert: "{certificate from (5)}"
            privateKey: "{private key from (6)}"
            singleLogoutService:
                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"
        strict: true
        # You can configure the settings in `advanced_settings.json` of python3-saml in this node (see https://gitlab.com/psono/psono-server/-/issues/153#note_443264234)
        security:
            authnRequestsSigned: true
```

::: tip
Always restart the server after making changes in the `setting.yml`-file.
:::


## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.

Update your config.json similar to the one shown below.

```json
{
  ...
    "authentication_methods": ["SAML"],
    "saml_provider": [{
      "title": "Some text before the button. e.g. Company Login",
      "provider_id": 1,
      "button_name": "SAML SSO Login"
    }]
  ...
}
```

The variable authentication_methods restricts the allowed login methods. In the example above only SAML will be allowed
and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
to match the one that you used on your server.
