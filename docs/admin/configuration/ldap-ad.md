---
title: LDAP - AD
metaTitle: AD for LDAP Authentication | Psono Documentation
meta:
  - name: description
    content: Configuration of LDAP with Active Directory
---

# AD for LDAP Authentication

## Preamble

The EE server and client support the LDAP protocol that allows you to configure an external LDAP service for authentication.
This guide here will explain how to configure Psono Server to use an Active Directory LDAP. We assume that psono server can firewall / network wise access the LDAP Server / port.
In addition we assume that your webclient is running on https://example.com, the server is reachable with
`https://example.com/server` (e.g. `https://example.com/server/info/` shows you some nice json output).

::: tip
This feature is only available in the Enterprise Edition.
:::

## Server (settings.yaml)

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Standard configuration

    First lets configure the standard configuration part that is the same for all LDAP connections

	```yaml
	LDAP : [
      {
	    'LDAP_URL': 'YOUR_LDAP_URL',
	    'LDAP_DOMAIN': 'LDAP_DOMAIN',
	    'LDAP_BIND_DN': 'YOUR_LDAP_BIND_DN',
	    'LDAP_BIND_PASS': 'YOUR_LDAP_BIND_PASS',
	    'LDAP_SEARCH_USER_DN': 'YOUR_LDAP_SEARCH_USER_DN',
	    'LDAP_SEARCH_GROUP_DN': 'YOUR_LDAP_SEARCH_GROUP_DN',
        ...
      },
    ]
	```

	- Replace `YOUR_LDAP_URL` with the URL to your LDAP. e.g. ldaps://192.168.0.1:636
	- Replace `YOUR_LDAP_DOMAIN` with your LDAP's domain. e.g. example.com
	- Replace `YOUR_LDAP_BIND_DN` with the DN of a user in your LDAP that can browse the directory. e.g. CN=LDAPPsono,OU=UsersTech,OU=example.com,DC=example,DC=com
	- Replace `YOUR_LDAP_BIND_PASS` with the password fo the user specified with the LDAP_BIND_DN
	- Replace `YOUR_LDAP_SEARCH_USER_DN` with the root DN underneath legit user objects would reside.
	- Replace `YOUR_LDAP_SEARCH_GROUP_DN` with the root DN underneath legit group objects would reside.

2.  Adjust the setting.yml as shown below

    The specific part for LDAP AD looks like this.

	```yaml
	LDAP : [
      {
        ...
        'LDAP_ATTR_GUID': 'objectGUID',
        'LDAP_OBJECT_CLASS_USER': 'user',
        'LDAP_OBJECT_CLASS_GROUP': 'group',
        'LDAP_ATTR_USERNAME': 'sAMAccountName',
        'LDAP_ATTR_EMAIL': 'mail',
        'LDAP_ATTR_GROUPS': 'memberOf',
        'LDAP_MEMBER_OF_OVERLAY': True,
        'LDAP_ATTR_GROUP_MEMBER_ATTRIBUTE': 'uid',
        'LDAP_ATTR_MEMBERS': 'memberUid',
        ...
      },
    ]
	```


    ::: tip
    If you specified a `LDAP_SEARCH_USER_DN` or `LDAP_SEARCH_GROUP_DN` without `OU`, then this is considered a global catalog
    search and as such you will have to use port `3268` (instead of `389`) or `3269` (instead of `636`). More infos can be found here:
    [docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-2000-server/cc978012](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-2000-server/cc978012(v=technet.10))
    :::

3.  Test your integration

    To test the settings you can use the `testldap` command like shown below

	```bash
    docker run --rm \
      -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-server-enterprise:latest python3 ./psono/manage.py testldap username@something.com thePassWord
	```


    ::: tip
    Once you have a working configuration don't forget to restart the server.
    :::

## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.

1.  Edit config.json

    Update your config.json similar to the one shown below.

	```json
	{
	  ...
      "authentication_methods": ["LDAP"],
	  ...
	}
	```

	The variable authentication_methods restricts the allowed login methods. In the example above only LDAP will be allowed.

