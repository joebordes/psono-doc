---
title: Disable browser password manager
metaTitle: Disable browser password manager | Psono Documentation
meta:
  - name: description
    content: How to disable the built in password manager and prevent autofill / autocomplete.
---

# Disable browser's built in password manager

## Preamble

Browsers ship with their own password manager and offer users to store passwords automatically. This guide will explain
how to disable the password managers of the most prominent password managers as they are most of the time incompatible
with Psono or at least irritating to user's who cannot distinguish the displayed messages.

## Google Chrome

### Disable Chrome's autofill option by:

1)  Clicking on the `three dots` at the top right

    ![Main menu chrome](/images/user/other_disable_browser_password_manager/chrome_menu.png)

2)  Click on settings

3)  Under `Autofill` click `Passwords`

4)  Toggle `Offer to save passwords` to `Off`


### Clear previous stored data:

1)  Clicking on the `three dots` at the top right

    ![Main menu chrome](/images/user/other_disable_browser_password_manager/chrome_menu.png)

2)  Click on `History` and then `History` again

3)  Click in the left menu on `Clear browsing data`

4)  Go to `Advanced`

    Choose `All Time`, check `Passwords and other sign-in data` and `Autofill form data`

    ![Chrome clear browsing data](/images/user/other_disable_browser_password_manager/chrome_clear_browsing_data.png)

5)  Confirm by clicking `Clear data`



## Microsoft Edge

### Disable Edge's autofill option by:

1)  Clicking on the `three dots` at the top right

    ![Main menu edge](/images/user/other_disable_browser_password_manager/edge_menu.png)

2)  Click on settings

3)  Go to `Profiles`

4)  click `Passwords` and Toggle `Offer to save passwords` to `Off` and go back

5)  click `Payment info` and Toggle `Save and fill payment info` to `Off` and go back

6)  click `Personal info` and Toggle `Save and fill personal info` to `Off` and go back


### Clear previous stored data:

1)  Clicking on the `three dots` at the top right

    ![Main menu edge](/images/user/other_disable_browser_password_manager/edge_menu.png)

2)  Click on settings

3)  Go to `Privacy, search, and services`

4)  In the `Clear browsing data` section choose `Choose what to clear`

    Choose `All Time`, check `Passwords` and `Autofill form data (includes forms and cards)`

    ![Edge Clear browsing data](/images/user/other_disable_browser_password_manager/edge_clear_browsing_data.png)

5)  Confirm by clicking `Clear now`



## Firefox

### Disable Firefox's autofill option by:

1)  Clicking on the `three bars` at the top right

    ![Firefox Menu](/images/user/other_disable_browser_password_manager/firefox_menu.png)

2)  Click on `Options`

3)  On the left choose `Privacy & Security`

4)  In the `Logins and Passwords` section uncheck `Ask to save logins and passwords for websites`


### Clear previous stored data:

1)  Clicking on the `three bars` at the top right

    ![Firefox Menu](/images/user/other_disable_browser_password_manager/firefox_menu.png)

2)  Click on `Options`

3)  On the left choose `Privacy & Security`

4)  In the `History` click on the `Clear History...` button

    ![Firefox privacy security history](/images/user/other_disable_browser_password_manager/firefox_privacy_security_history.png)

5)  Select `Everything` and `For & Search History`

    ![Firefox clear all history](/images/user/other_disable_browser_password_manager/firefox_clear_all_history.png)


## Safari

### Disable Safari's autofill option by:

1)  Clicking on the `Safari` at the top

2)  Click on `Preferences`

3)  Select `Autofill`

4)  Uncheck `Using info from my Contacts Card / Address Book Card` and `Other Forms`

5)  Click `Done`


### Clear previous stored data:

1)  Clicking on the `Safari` at the top

2)  Click on `Preferences`

3)  Select `Autofill`

3)  Click on `Edit` next to `User names and Passwords`

4)  Click `Remove All`

5)  Click `Done`